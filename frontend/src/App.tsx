import { useEffect, useState } from "react";

import { Socket } from "socket.io-client";

import Game from "./components/Game";
import Login from "./components/Login";
import { getSocketConnection } from "./shared/utils";
import { User, OutgoindEventType } from "./shared/types";

function App() {
  const [connected, setConnected] = useState<boolean>(false);
  const [user, setUser] = useState<User | null>(null);
  const [socket] = useState<Socket | null>(getSocketConnection());

  useEffect(() => {
    socket?.on("connect", () => {
      setConnected(true);
    });
    socket?.on("disconnect", () => {
      setConnected(false);
    });
    return () => {
      socket?.off("connect");
      socket?.off("disconnect");
      socket?.disconnect();
    };
  }, [socket]);

  const onLogin = (name: string) => {
    if (socket?.connected) {
      setUser({ name, socketId: socket.id });
      socket?.emit(OutgoindEventType.LOGIN, { username: name });
    }
  };

  if (!user && connected) {
    return <Login onLogin={onLogin} />;
  }
  if (user && connected && socket) {
    return (
      <Game
        socket={socket}
        user={user}
        onLogout={() => {
          setUser(null);
          socket.disconnect();
          socket.connect();
        }}
      />
    );
  } else {
    return (
      <div className="text-2xl text-brand h-screen flex items-center justify-center">
        Trying to Connect .... 🧐
      </div>
    );
  }
}

export default App;
