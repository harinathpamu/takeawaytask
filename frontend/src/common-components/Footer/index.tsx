import FooterLogo from "../../assets/footer-logo.svg";

function Footer() {
  return (
    <footer className="px-4 py-2 bg-cold-very-dark shadow-lg">
      <div className="flex items-center justify-between space-x-4">
        <img alt="brandlogo" src={FooterLogo} className="h-10 flex-shrink-0" />
        <p className="text-xs font-normal text-gray-400">
          © {new Date().getFullYear()} Takeaway.com
        </p>
      </div>
    </footer>
  );
}

export default Footer;
