import BrandLogo from "../../assets/logo.svg";

interface HeaderProps {
  username: string;
  onLogout: () => void;
}

function Header(props: HeaderProps) {
  const { username, onLogout } = props;
  return (
    <header className="px-4 py-2 bg-brand flex justify-between items-center shadow-lg">
      <div className="flex items-center space-x-4">
        <img alt="brandlogo" src={BrandLogo} className="h-10 w-10" />
        <div className="flex flex-col ">
          <p className="text-lg font-bold text-white">Game</p>
          <p className="text-sm text-white font-normal">
            Win the game or win the job
          </p>
        </div>
      </div>
      <div className="font-bold text-white flex items-center gap-2">
        <span>{username}</span>
        <button
          type="submit"
          onClick={() => onLogout?.()}
          className="px-3 py-1.5 rounded text-brand bg-white text-sm shadow"
        >
          EXIT GAME
        </button>
      </div>
    </header>
  );
}

export default Header;
