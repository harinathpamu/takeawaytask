import BrandLogo from "../../assets/logo.svg";
import Person from "../../assets/person.svg";

interface ChatMessageProps {
  selectedNumber: number;
  number: number;
  result: number;
  currentUserName: string;
  name: string;
}

function ChatMessage(props: ChatMessageProps) {
  const { selectedNumber, number, result, name, currentUserName } = props;
  const isMe = name === currentUserName;
  return (
    <div className={`flex gap-2 ${!isMe ? "flex-row-reverse" : ""}`}>
      <img
        alt="brandlogo"
        src={isMe ? BrandLogo : Person}
        className={`h-10 w-10 rounded-full bg-cold-very-dark flex  items-center justify-center `}
      />
      <div className="space-y-2">
        <div
          className={`h-14 w-14 ${
            isMe ? "bg-cold-very-dark " : "bg-blue ml-auto"
          } rounded-full font-bold text-lg flex items-center justify-center text-white`}
        >
          {selectedNumber}
        </div>
        <div className="px-4 py-3 text-xs bg-grey">{`[ ( ${selectedNumber} + ${number} ) / 3 ] = ${result}`}</div>
        <div className="px-4 py-3 text-xs bg-grey">{result}</div>
      </div>
    </div>
  );
}

export default ChatMessage;
