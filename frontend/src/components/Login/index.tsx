import { useState } from "react";

interface LoginProps {
  onLogin: (name: string) => void;
}

function Login(props: LoginProps) {
  const { onLogin } = props;
  const [name, setName] = useState("");
  return (
    <div className="h-screen flex items-center justify-center">
      <form
        onSubmit={(event) => {
          event.preventDefault();
          if (name?.trim()) {
            onLogin?.(name);
          }
        }}
        className="max-w-[250px] flex flex-col space-y-3 bg-white rounded p-3 shadow-lg w-full"
      >
        <h1 className="text-lg font-bold text-brand">ENTER NAME</h1>
        <input
          className="px-3 py-1 focus:outline-none bg-gray-100 rounded border font-light"
          value={name}
          placeholder="Name"
          required
          onChange={(event) => {
            setName(event.target.value);
          }}
        />
        <button
          type="submit"
          className="px-3 py-1.5 rounded text-white bg-brand text-xs shadow"
        >
          PLAY
        </button>
      </form>
    </div>
  );
}

export default Login;
