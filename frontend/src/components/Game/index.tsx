import { useEffect, useState, useRef } from "react";

import ChatMessage from "../ChatMessage";
import ChooseNumber from "../ChooseNumber";
import WinImage from "../../assets/win.svg";
import LoseImage from "../../assets/lose.svg";
import Header from "../../common-components/Header";
import Footer from "../../common-components/Footer";
import { getSendNumber } from "../..//shared/utils";
import {
  Room,
  RandomNumberAfter,
  RandomNumberFirst,
  User,
  Turn,
  GameProps,
  GameOver,
  IncomingEventType,
  OutgoindEventType,
  Chat,
  GAME_STATE,
  OnReady,
} from "../../shared/types";

function Game(props: GameProps) {
  const { socket, user, onLogout } = props;

  const [rooms, setRooms] = useState<Room[]>([]);
  const [message, setMessage] = useState<string>("");
  const [onReady, setOnReady] = useState<boolean>(false);
  const [choose, setChoose] = useState<boolean>(false);
  const lastResultNumberRef = useRef<number | null>(null);
  const [chatItems, setChatItems] = useState<Chat[]>([]);
  const [toggle, setToggle] = useState<boolean>(true);

  const [gameOver, _setGameOver] = useState<GameOver | null>(null);
  const gameOverRef = useRef<GameOver | null>(null);

  const [room, _setRoom] = useState<Room | null>(null);
  const roomRef = useRef<Room | null>(room);

  const listRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    socket.on(IncomingEventType.MESSAGE, (data) => {
      if (data) {
        setMessage(data.message);
        setTimeout(() => {
          setMessage("");
        }, 5000);
      }
    });

    socket.on(IncomingEventType.ON_READY, (data: OnReady) => {
      if (data) {
        if (data.state === false) {
          setChatItems([]);
          setChoose(false);
          setRoom(null);
          setToggle(true);
        }
        setOnReady(data.state);
      }
    });

    socket.on(
      IncomingEventType.RANDOM_NUMBER,
      (data: RandomNumberFirst | RandomNumberAfter) => {
        if (data) {
          if (!gameOverRef.current) {
            if ("user" in data && !data.isFirst) {
              if (lastResultNumberRef.current !== null) {
                const newChat = {
                  selectedNumber: +data.selectedNumber,
                  number: lastResultNumberRef.current,
                  result: +data.number,
                  name: data.user,
                };

                setChatItems((state) => {
                  return [...state, newChat];
                });
              }
            } else {
              if (roomRef?.current && roomRef.current.type === "cpu") {
                setChoose(true);
              }
            }
            lastResultNumberRef.current = +data.number;
          }
        }
      }
    );

    socket.on(IncomingEventType.ACTIVATE_YOUR_TURN, (data: Turn) => {
      if (!gameOverRef.current && data) {
        if (data.state === GAME_STATE.PLAY && data.user === null) {
          setToggle(false);
          setChoose(true);
        } else if (
          (data.state === GAME_STATE.PLAY && socket.id === data.user) ||
          (data.state === GAME_STATE.WAIT && socket.id !== data.user)
        ) {
          setChoose(true);
        } else if (data.state === GAME_STATE.WAIT && socket.id === data.user) {
          setChoose(false);
        } else {
          setToggle(false);
          setChoose(false);
        }
      }
    });

    socket.on(IncomingEventType.GAME_OVER, (data: GameOver) => {
      if (!gameOverRef.current && data) {
        setChoose(false);
        setGameOver(data);
      }
    });

    fetchRooms();

    return () => {
      socket.off(IncomingEventType.RANDOM_NUMBER);
      socket.off(IncomingEventType.ACTIVATE_YOUR_TURN);
      socket.off(IncomingEventType.GAME_OVER);
      socket.off(IncomingEventType.MESSAGE);
      socket.off(IncomingEventType.ON_READY);
    };
  }, [socket]);

  const fetchRooms = () => {
    fetch(`${process.env.REACT_APP_FAKEDB_URL}/rooms`)
      .then((res) => res.json())
      .then((data) => {
        setRooms(data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const setRoom = (room: Room | null) => {
    roomRef.current = room;
    _setRoom(room);
  };

  const setGameOver = (gameOver: GameOver | null) => {
    gameOverRef.current = gameOver;
    _setGameOver(gameOver);
  };

  const onLetsPlay = () => {
    if (socket?.connected) {
      socket?.emit(OutgoindEventType.LETS_PLAY);
      setToggle(false);
    }
  };

  const newGame = () => {
    setChatItems([]);
    setChoose(false);
    setToggle(true);
    setGameOver(null);
    onLetsPlay();
  };

  const onSendNumber = (number: number, selectedNumber: number) => {
    if (socket?.connected) {
      socket?.emit(OutgoindEventType.SEND_NUMBER, { number, selectedNumber });
    }
  };

  const onJoinRoom = (fullRoom: Room, user: User) => {
    if (socket?.connected) {
      setRoom(fullRoom);
      socket?.emit(OutgoindEventType.JOIN_ROOM, {
        username: user.name,
        room: fullRoom.name,
        roomType: fullRoom.type,
      });
    }
  };

  const onChooseNumber = (choosedNumber: number) => {
    if (lastResultNumberRef.current !== null) {
      const result = getSendNumber(choosedNumber, lastResultNumberRef.current);
      if (result !== null && !gameOverRef.current) {
        onSendNumber(result, choosedNumber);
      }
    }
  };

  const leaveRoom = () => {
    if (socket?.connected) {
      setGameOver(null);
      socket?.emit(OutgoindEventType.LEAVE_ROOM);
    }
  };

  useEffect(() => {
    if (choose) {
      const lastChild = listRef.current?.lastElementChild;
      lastChild?.scrollIntoView({
        behavior: "smooth",
      });
    }
  }, [choose]);

  const GameOver = (gameOver: GameOver, user: User) => {
    if (gameOver.isOver) {
      const isMe = gameOver.user === user.name;
      return (
        <div className="bg-black bg-opacity-50 inset-0 flex items-center justify-center absolute">
          <div className="flex flex-col space-y-4 items-center justify-center">
            <img
              alt="gameStatusImage"
              src={isMe ? WinImage : LoseImage}
              className="w-44"
            />
            <div className="text-5xl font-bold text-white">
              You {isMe ? "Won" : "Lose"}
            </div>
            <button
              onClick={newGame}
              className="text-blue px-8 py-3 rounded-full bg-white text-base font-bold shadow-lg"
            >
              New game
            </button>
          </div>
        </div>
      );
    }
  };

  const Rooms = (rooms: Room[], room: Room | null) => {
    return rooms?.map((it, index) => {
      let isCurrentRoom = room?.name === it.name;
      return (
        <div
          className={`md:p-5 space-x-2 p-2  ${
            isCurrentRoom ? "bg-blue" : "bg-white"
          } flex justify-between items-center`}
          key={index}
        >
          <p
            role="button"
            className={`text-sm ${
              isCurrentRoom ? "text-white" : "text-info"
            } font-bold`}
          >
            {it.name}
          </p>

          <span
            className={`rounded ${
              isCurrentRoom ? "bg-white text-blue" : "bg-blue text-white"
            }  p-1 text-xs`}
            role="button"
            onClick={() => {
              if (room) {
                if (isCurrentRoom) {
                  leaveRoom();
                } else {
                  window.confirm(
                    `Please leave '${room.name}' room to join another room`
                  );
                }
              } else {
                onJoinRoom(it, user);
              }
            }}
          >
            {isCurrentRoom ? "Leave" : "Join"} Room{" "}
          </span>
        </div>
      );
    });
  };

  const Message = (msg: string) => {
    return (
      <div>
        <p className="text-xs text-brand capitalize">{msg}</p>
      </div>
    );
  };

  const LetsPlay = (onLetsPlay: () => void) => {
    return (
      <div className="h-full  flex items-center justify-center">
        <button
          onClick={onLetsPlay}
          className="px-3 py-1.5 rounded text-white bg-brand text-sm shadow"
        >
          Let's Play
        </button>
      </div>
    );
  };

  const ChatMessages = (chatItems: Chat[]) => {
    return chatItems?.map((chatItem, index) => {
      return (
        <ChatMessage {...chatItem} currentUserName={user.name} key={index} />
      );
    });
  };

  return (
    <div>
      <Header username={user.name} onLogout={onLogout} />
      <div className="lg:container lg:mx-auto h-[calc(100vh_-_120px)] overflow-y-auto">
        <div className="h-full flex flex-col md:flex-row overflow-auto">
          <div className="w-full md:w-1/3 lg:w-1/4 md:h-full md:p-5 bg-grey md:space-y-4 flex md:inline-block md:flex-none">
            <div className="md:space-y-4">
              <p className="text-sm text-info font-bold">Choose your game</p>
              <div className="flex gap-2 md:items-center md:block md:flex-none">
                {Rooms(rooms, room)}
              </div>
            </div>
            {message && Message(message)}
          </div>
          <div
            className="bg-white w-full h-full overflow-y-auto relative space-y-4 p-4"
            ref={listRef}
          >
            {gameOver && GameOver(gameOver, user)}
            {onReady && toggle && LetsPlay(onLetsPlay)}
            {ChatMessages(chatItems)}
            {choose && onReady && (
              <ChooseNumber onChooseNumber={onChooseNumber} />
            )}
            {!gameOver && !onReady && toggle && !choose && !room && (
              <div className="h-full flex items-center justify-center">
                <span className="text-brand text-xl font-bold">
                  Join Room To Play
                </span>
              </div>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Game;
