import { forwardRef, Ref } from "react";

interface ChooseNumberProps {
  onChooseNumber: (val: number) => void;
}

function ChooseNumber(props: ChooseNumberProps, ref: Ref<HTMLDivElement>) {
  const { onChooseNumber } = props;
  const numbers = [-1, 0, +1];
  return (
    <div className="mx-auto" ref={ref}>
      <div className="flex justify-center gap-2 items-center my-4">
        {numbers?.map((number) => {
          return (
            <button
              key={number}
              data-testid="number"
              onClick={() => onChooseNumber?.(number)}
              className="h-14 w-14 bg-white shadow rounded-full font-bold text-lg flex items-center justify-center text-blue border"
            >
              {number}
            </button>
          );
        })}
      </div>
      <p className="text-xs text-brand text-center">
        Click on the number <br />
        to choose
      </p>
    </div>
  );
}

export default forwardRef(ChooseNumber);
