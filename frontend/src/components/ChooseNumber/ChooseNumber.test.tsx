import { render, screen } from "@testing-library/react";
import ChooseNumber from ".";

it("should render all numbers -1 0 1", () => {
  const onChooseNumber = jest.fn();
  render(<ChooseNumber onChooseNumber={onChooseNumber} />);
  const numbers = screen.getAllByTestId("number");
  const allTextContent = numbers?.map((it) => it.textContent)?.join("");
  expect(allTextContent).toEqual("-101");
});

it("should be able choose numbers -1 0 1", () => {
  const onChooseNumber = jest.fn();
  render(<ChooseNumber onChooseNumber={onChooseNumber} />);
  const numbers = screen.getAllByTestId("number");
  numbers[0].click();
  expect(onChooseNumber).toHaveBeenCalledWith(-1);
});
