import { io } from "socket.io-client";

export function getSendNumber(number1: number, number2: number): number {
  const sum = number1 + number2;
  if (sum % 3 === 0) {
    return sum / 3;
  } else {
    return sum;
  }
}

export function getSocketConnection() {
  const socketURL = process.env.REACT_APP_SOCKET_URL;
  if (socketURL) {
    return io(`${socketURL}`);
  } else {
    return null;
  }
}
