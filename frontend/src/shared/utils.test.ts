import { getSendNumber } from "./utils";

it("getSendNumber for 0 and 3 is 1", () => {
  expect(getSendNumber(0, 3)).toEqual(1);
});

it("getSendNumber for 4 and 4 is 8", () => {
  expect(getSendNumber(4, 4)).toEqual(8);
});
