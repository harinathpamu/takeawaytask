import { Socket } from "socket.io-client";

export interface Room {
  id: string;
  name: string;
  owner: string;
  type: string;
}

export interface RandomNumberAfter {
  number: number;
  isFirst: boolean;
  user: string;
  selectedNumber: number;
  isCorrectResult: boolean;
}

export interface RandomNumberFirst {
  number: number;
  isFirst: boolean;
}

export interface User {
  name: string;
  socketId: string;
}

export interface Turn {
  user: string;
  state: string;
}

export interface OnReady {
  state: boolean;
}

export interface GameProps {
  socket: Socket;
  user: User;
  onLogout: () => void;
}

export interface GameOver {
  user: string;
  isOver: boolean;
}

export enum IncomingEventType {
  MESSAGE = "message",
  ON_READY = "onReady",
  RANDOM_NUMBER = "randomNumber",
  ACTIVATE_YOUR_TURN = "activateYourTurn",
  GAME_OVER = "gameOver",
}

export enum GAME_STATE {
  PLAY = "play",
  WAIT = "wait",
}

export enum OutgoindEventType {
  LOGIN = "login",
  LETS_PLAY = "letsPlay",
  JOIN_ROOM = "joinRoom",
  SEND_NUMBER = "sendNumber",
  LEAVE_ROOM = "leaveRoom",
}

export interface Chat {
  selectedNumber: number;
  number: number;
  result: number;
  name: string;
}
