module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors:{
        'brand':'#FF8000',
        'info':'#205A6D',
        'grey':'#F8F5F2',
        "blue":"#1574F5",
        'cold-very-dark':'#0A3847'
      }
    },
  },
  plugins: [],
}