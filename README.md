![Demo](https://gitlab.com/harinathpamu/takeawaytask/-/raw/master/Image.png)

# Getting Started

First clone the repository in your local machine.

In your root folder where the repository is cloned, run the following commands:

### Install dependencies

- `cd ./backend/wss` now wss directory as root
- `npm install`
- `cd ./frontend` now frontend directory as root
- `npm install`

### Start backend

- Navigate to `./backend/wss` directory
- Start the fake DB JsonServer: `npm run start:server`
- Open a new terminal and type: `npm run start`.

### Start frontend

- Navigate to `./frontend` directory
- Open a new terminal and type:: `npm run start`
- Open http://localhost:3000/ and start playing by login

# Game Rules

When a player starts, they incept a random (whole) number and send it to the second player as an approach of starting the game. The receiving player can then choose between adding one of {-1,0,1} in order to get to a number that is divisible by 3. The resulting whole number is then sent back to the original sender.
​
The same rules are applied until one player reaches the number 1 (after the division)

# Wireframe

[https://www.figma.com/file/F914dSnSz01H1D1EY64ZXO/Game_of_three?node-id=0%3A1](https://www.figma.com/file/F914dSnSz01H1D1EY64ZXO/Game_of_three?node-id=0%3A1)
